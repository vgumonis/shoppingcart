<?php

namespace ShoppingCart\src\Controllers;

use ShoppingCart\src\Views\PageNotFoundView;

class ErrorController
{
    public function pageNotFound()
    {
       new PageNotFoundView();
    }
            
        
        
}