<?php

namespace ShoppingCart\src\Controllers;

use ShoppingCart\src\Views\MainView;

class MainController
{
    public function __construct() 
    {
        session_start();
        
        if (empty($_SESSION['basket'])) {
            $_SESSION['basket'] = ["BlueShoe" => 0, "BlackShoe" => 0, "PinkShoe" => 0, "Sum" => 0];
        }
         new MainView();   
    }
    
    
}