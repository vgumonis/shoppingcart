<?php

namespace ShoppingCart\src\Controllers;

class DeleteItemController
{
    public function __construct() 
    {
       session_start();
       
      $body = file_get_contents("php://input", true);
      
      switch ($body[3]) {
           case "1":
            $_SESSION['basket']["BlueShoe"] -=1;
            $_SESSION['basket']['Sum'] -= 80;
            break;
        case "2":
            $_SESSION['basket']["BlackShoe"] -=1;
            $_SESSION['basket']['Sum'] -= 50;
            break;
        case "3":
           $_SESSION['basket']["PinkShoe"] -=1;
           $_SESSION['basket']['Sum'] -= 100;
            break;
        case "a":
            $_SESSION['basket'] = ["BlueShoe" => 0, "BlackShoe" => 0, "PinkShoe" => 0, "Sum" => 0];
            break;   
      }
       
      self::CheckIfLessThanZero();
    }
    
    
    private static function CheckIfLessThanZero()
    {
     ($_SESSION['basket']['BlueShoe'] > 0) ? :  $_SESSION['basket']['BlueShoe'] = 0 ;
     ($_SESSION['basket']['BlackShoe'] > 0) ? :  $_SESSION['basket']['BlackShoe'] = 0 ;
     ($_SESSION['basket']['PinkShoe'] > 0) ? :  $_SESSION['basket']['PinkShoe'] = 0 ;
     ($_SESSION['basket']['Sum'] > 0) ? :  $_SESSION['basket']['Sum'] = 0 ;
    }
}
