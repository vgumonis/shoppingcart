<?php

namespace ShoppingCart\src\Views;

class CatalogView
{
    public function __construct() 
    {
 
        ?>
    <h2> Choose an item.</h2>

    <div class="flex-container">
        
    <div class="item">   
    <img src="src/images/blue_shoe.jpg" >
    <h2 class="item-name">Blue Shoe </h2>
    <h3 class="price"> 80 EUR </h3> 
    <div class="flex-container">
        <button id="add-blue" type="button" class="btn btn-primary">Add to Cart</button>
        <button id="remove-blue"type="button" class="btn btn-primary ">Remove from cart </button>
    </div>
    <h3 id="added-blue"> Item added!</h3>
    <h3 id="removed-blue"> Item removed! </h3>
    </div>
    
    <div class="item">   
    <img src="src/images/black_shoe.jpg" >
    <h2 class="item-name">Black Shoe </h2>
    <h3 class="price"> 50 EUR </h3> 
    <div class="flex-container">
    <button id="add-black" type="button" class="btn btn-primary ">Add to Cart</button>
    <button id="remove-black" type="button" class="btn btn-primary">Remove from cart </button>
    </div>
     <h3 id="added-black"> Item added!</h3>
    <h3 id="removed-black"> Item removed! </h3>
    </div>
    
    <div class="item">   
    <img src="src/images/pink_shoe.jpg" >
    <h2 class="item-name">Pink Shoe </h2>
    <h3 class="price"> 100 EUR </h3> 
    <div class="flex-container">
    <button id="add-pink" type="button" class="btn btn-primary">Add to Cart</button>
    <button id="remove-pink" type="button" class="btn btn-primary">Remove from cart </button>
    </div>
      <h3 id="added-pink"> Item added!</h3>
    <h3 id="removed-pink"> Item removed! </h3>
    </div>

    </div>

  <?php      
    }
 
}

