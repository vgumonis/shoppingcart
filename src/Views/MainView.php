<?php
namespace ShoppingCart\src\Views;

use ShoppingCart\src\Views\CatalogView;
use ShoppingCart\src\Views\BasketView;


class MainView
{
    public  function __construct() {
      
      ?>
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Shopping Cart</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                  crossorigin="anonymous">
            <link rel="stylesheet" href="src/Style/styles.css">
            <script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
        </head>
        <body>
            <h1 id="logo"> Welcome to Shopping Cart!</h1>    
        <?php 
        new CatalogView();
        include 'BasketView.php' ;
        ?>
            
        <script src="src\Scripts\script.js"></script>
        </body>
        </html>
        <?php


  
      
        
    } 
    
    
    
    
}
