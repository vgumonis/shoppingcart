<?php

require_once 'vendor/autoload.php';

use ShoppingCart\src\Controllers\MainController;
use ShoppingCart\src\Controllers\ErrorController;

if(file_exists("src/Controllers/MainController.php")){
    
    new MainController();
    
} else {

    $error = new ErrorController();
    $error->pageNotFound();
}

